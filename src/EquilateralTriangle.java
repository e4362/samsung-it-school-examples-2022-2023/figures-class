public class EquilateralTriangle extends Triangle2 {
    public EquilateralTriangle(double a) {
        super(a, a, a);
    }
    @Override
    public String toString() {
        return "Equilateral Triangle{" +
                "a=" + super.a +
                "}.\n" +
                "Area: " + getArea() + "\n" +
                "Perimetr: " + getPerimetr();
    }

    @Override
    public double getArea() {
        return super.a*super.a * Math.sqrt(3) / 4;
    }
}
