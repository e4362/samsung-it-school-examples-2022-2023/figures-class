import java.util.Random;

public class Point {
    private double x, y;
    static int counter = 0;

    static int i = 2;
    static {
        for (int k = 0; k < 10; k++){
            i *= 2;
        }
    }

    public Point(double x, double y) {
        counter++;
        setX(x);
        setY(y);
    }

    public Point() {

    }

    public Point(Point p) {
        this.x = p.x;
        this.y = p.y;
    }
    public void setX(double x) {
        if (x >= 0 && x <= 1000){
            this.x = x;
        }
        else {
            System.out.println("Координата X не входит в указанный диапазон");
        }
    }

    public void setY(double y) {
        if (y >= 0 && y <= 1000){
            this.y = y;
        }
        else {
            System.out.println("Координата Y не входит в указанный диапазон");
        }
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    double getDistance(Point p){
        return Math.sqrt((this.x - p.x) * (this.x - p.x) + (this.y - p.y) * (this.y - p.y));
    }

    static double getDistance(Point p1, Point p2){
        return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) +
                (p1.y - p2.y) * (p1.y - p2.y));
    }
}
