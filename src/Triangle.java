public class Triangle {
    private Point a, b, c;

    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getPerimetr(){
        return a.getDistance(b) + b.getDistance(c) + c.getDistance(a);
    }

    public double getArea(){
        double p = getPerimetr() / 2;
        double s1 = a.getDistance(b);
        double s2 = b.getDistance(c);
        double s3 = c.getDistance(a);
        return Math.sqrt(p * (p - s1) * (p - s2) * (p - s3));
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                "}.\n" +
                "Area: " + getArea() + "\n " +
                "Perimetr: " + getPerimetr();
    }
}
