public class Triangle2 {
    double a, b, c;

    public Triangle2(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getPerimetr(){
        return a + b + c;
    }

    public double getArea(){
        double p = getPerimetr() / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                "}.\n" +
                "Area: " + getArea() + "\n " +
                "Perimetr: " + getPerimetr();
    }
}
